from django.db import models


# Create your models here.
class Realtor(models.Model):
    title = models.CharField(max_length=250)
    phone = models.CharField(max_length=250,default='')
    email = models.CharField(max_length=250, default='')
    description = models.CharField(max_length=250, default='')
    yearsInField = models.IntegerField(default=0)
    image = models.ImageField(upload_to='realtor/%Y/%m/%d/', null=True)

    def __str__(self):
        return self.title
