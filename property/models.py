from django.db import models
from realtors.models import Realtor


class Property(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    price = models.CharField(max_length=200)
    kitchen = models.CharField(max_length=200)
    bedroom = models.CharField(max_length=200)
    hallway = models.CharField(max_length=200)
    roadInFront = models.CharField(max_length=200)
    featured = models.BooleanField(default=True)
    coverImage = models.ImageField(upload_to='property/%Y/%m/%d/', null=True)
    cardImage = models.ImageField(upload_to='property/%Y/%m/%d/', null=True)
    realtor = models.ForeignKey(Realtor, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

