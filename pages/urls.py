from django.urls import path
from . import views

urlpatterns = [
     path('', views.index, name='home'),
     path('about', views.about, name='about'),
     path('property/<int:property_id>', views.property, name='property_details'),
]
