from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from property.models import Property


# Create your views here.


def index(request):
    featured_property = Property.objects.filter(featured=True)[:4]
    latest_property = Property.objects.order_by('id')[:4]
    return render(request, 'pages/index.html', {
        'featured': featured_property,
        'latest': latest_property
    })
    # return HttpResponse('this is the home route')


def about(request):
    propertyList = Property.objects.all
    return render(request, 'pages/about.html', {'title': 'Shyam', 'properties': propertyList})


def property(request, property_id):
    property_details = get_object_or_404(Property, pk=property_id)
    context = {
        "property": property_details,
    }
    return render(request, 'pages/property.html', context)


def temp(request):
    if request.method == 'POST':
        text = request.POST['test']
    context = {
        "property": text
    }
    return render(request, 'pages/test.html', context)
